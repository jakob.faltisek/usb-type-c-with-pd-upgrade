# USB 3.1 Type C to Type A Adapter with PD

This board is an universal USB Type C to Type A (male) converter that is capable of USB 3.1 Gen 2 data rates.
The PCB is able to extract the USB PD signal and output the voltage to a screw terminal.


## Usage examples

### Implemented in current revision

* Handle the USB PD negotiation and extract power from a USB PD power supply
* Act as a USB 3.1 USB Type A to Type C Adapter
* Act as a passive USB 2 Type A to Type C Adapter
* Upgrade a Laptop that has a USB 3 Type A port to a Type C port and charge the Laptop via 20V USB PD (DC plug connected to the screw terminal)

Note: This features are supported by the hardware revision v1.0, the firmware is not implemented yet.

### Planned for future revisions

* Act as a DFP to provide USB PD power to a device (the voltage higher than 5V is inserted via the screw terminals)
* Automatically switch between the different roles (UFP and DFP) depenting on the connected devices


## External links
This project was featured in [this Hackaday post](https://hackaday.com/2019/06/30/extracting-power-from-usb-type-c/) and is also documented on [Hackaday.io](https://hackaday.io/project/164707-usb-31-type-c-to-type-a-adapter-with-pd).  
The hardware of this project can be viewed on [CADLAB.io](https://cadlab.io/project/1765).  